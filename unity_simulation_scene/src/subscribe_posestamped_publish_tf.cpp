#include <ros/ros.h>
#include <tf/transform_broadcaster.h>   //tf
#include <geometry_msgs/PoseStamped.h>	// pose_stamped

tf::TransformBroadcaster *tb;
std::string child_frame;

void PoseMsgCallback(const geometry_msgs::PoseStamped::ConstPtr& msg){
    
    //ROS_INFO("Pose Stamped received. Publish TF.");

    geometry_msgs::TransformStamped pose_stamped_trans;
    pose_stamped_trans.header.stamp = msg->header.stamp;
    pose_stamped_trans.header.frame_id = msg->header.frame_id;
    pose_stamped_trans.child_frame_id = child_frame; //given by the ROS parameter of this node
    
    pose_stamped_trans.transform.translation.x = msg->pose.position.x;
    pose_stamped_trans.transform.translation.y = msg->pose.position.y;
    pose_stamped_trans.transform.translation.z = msg->pose.position.z;
    pose_stamped_trans.transform.rotation.x = msg->pose.orientation.x;
    pose_stamped_trans.transform.rotation.y = msg->pose.orientation.y;
    pose_stamped_trans.transform.rotation.z = msg->pose.orientation.z;
    pose_stamped_trans.transform.rotation.w = msg->pose.orientation.w;
    tb->sendTransform(pose_stamped_trans);    
}


int main(int argc, char** argv){
    
	ros::init(argc, argv, "subscribe_posestamped_publish_tf");
        
	ros::NodeHandle n;
  ros::NodeHandle pp("~"); //private parameters
  
  pp.param<std::string>("child_frame",child_frame, "bobcat_base");  //should be changed later to "odom" 

  tb = new tf::TransformBroadcaster();
  ros::Subscriber pose_stamped_sub = n.subscribe<geometry_msgs::PoseStamped>("global_localization", 100, &PoseMsgCallback); 

  ros::spin();   
        
  return 0;
};